# Generated @ 2023-05-23T09:25:09+02:00
# Command line:
#   /usr/home/lbartoletti/.nimble/pkgs/nimterop-0.6.13/nimterop/toast --pnim --dynlib libSFCGAL.so lib/sfcgal_c.h

{.push hint[ConvFromXtoItselfNotNeeded]: off.}
import macros

macro defineEnum(typ: untyped): untyped =
  result = newNimNode(nnkStmtList)

  # Enum mapped to distinct cint
  result.add quote do:
    type `typ`* = distinct cint

  for i in ["+", "-", "*", "div", "mod", "shl", "shr", "or", "and", "xor", "<", "<=", "==", ">", ">="]:
    let
      ni = newIdentNode(i)
      typout = if i[0] in "<=>": newIdentNode("bool") else: typ # comparisons return bool
    if i[0] == '>': # cannot borrow `>` and `>=` from templates
      let
        nopp = if i.len == 2: newIdentNode("<=") else: newIdentNode("<")
      result.add quote do:
        proc `ni`*(x: `typ`, y: cint): `typout` = `nopp`(y, x)
        proc `ni`*(x: cint, y: `typ`): `typout` = `nopp`(y, x)
        proc `ni`*(x, y: `typ`): `typout` = `nopp`(y, x)
    else:
      result.add quote do:
        proc `ni`*(x: `typ`, y: cint): `typout` {.borrow.}
        proc `ni`*(x: cint, y: `typ`): `typout` {.borrow.}
        proc `ni`*(x, y: `typ`): `typout` {.borrow.}
    result.add quote do:
      proc `ni`*(x: `typ`, y: int): `typout` = `ni`(x, y.cint)
      proc `ni`*(x: int, y: `typ`): `typout` = `ni`(x.cint, y)

  let
    divop = newIdentNode("/")   # `/`()
    dlrop = newIdentNode("$")   # `$`()
    notop = newIdentNode("not") # `not`()
  result.add quote do:
    proc `divop`*(x, y: `typ`): `typ` = `typ`((x.float / y.float).cint)
    proc `divop`*(x: `typ`, y: cint): `typ` = `divop`(x, `typ`(y))
    proc `divop`*(x: cint, y: `typ`): `typ` = `divop`(`typ`(x), y)
    proc `divop`*(x: `typ`, y: int): `typ` = `divop`(x, y.cint)
    proc `divop`*(x: int, y: `typ`): `typ` = `divop`(x.cint, y)

    proc `dlrop`*(x: `typ`): string {.borrow.}
    proc `notop`*(x: `typ`): `typ` {.borrow.}


{.pragma: impsfcgal_cHdr,
  header: "../lib/sfcgal_c.h".}
{.pragma: impsfcgal_cDyn, dynlib: "libSFCGAL.so".}
{.experimental: "codeReordering".}
defineEnum(sfcgal_geometry_type_t) ## ```
                                   ##   Geometric types
                                   ##    @ingroup capi
                                   ## ```
const
  SFCGAL_TYPE_POINT* = (1).sfcgal_geometry_type_t ## ```
                                                  ##   TYPE_GEOMETRY            = 0, abstract
                                                  ## ```
  SFCGAL_TYPE_LINESTRING* = (2).sfcgal_geometry_type_t
  SFCGAL_TYPE_POLYGON* = (3).sfcgal_geometry_type_t
  SFCGAL_TYPE_MULTIPOINT* = (4).sfcgal_geometry_type_t
  SFCGAL_TYPE_MULTILINESTRING* = (5).sfcgal_geometry_type_t
  SFCGAL_TYPE_MULTIPOLYGON* = (6).sfcgal_geometry_type_t
  SFCGAL_TYPE_GEOMETRYCOLLECTION* = (7).sfcgal_geometry_type_t ## ```
                                                               ##   TYPE_CIRCULARSTRING      = 8,
                                                               ##          TYPE_COMPOUNDCURVE       = 9,
                                                               ##          TYPE_CURVEPOLYGON        = 10,
                                                               ##          TYPE_MULTICURVE          = 11, abstract
                                                               ##          TYPE_MULTISURFACE        = 12, abstract
                                                               ##          TYPE_CURVE               = 13, abstract
                                                               ##          TYPE_SURFACE             = 14, abstract
                                                               ## ```
  SFCGAL_TYPE_POLYHEDRALSURFACE* = (15).sfcgal_geometry_type_t ## ```
                                                               ##   TYPE_CIRCULARSTRING      = 8,
                                                               ##          TYPE_COMPOUNDCURVE       = 9,
                                                               ##          TYPE_CURVEPOLYGON        = 10,
                                                               ##          TYPE_MULTICURVE          = 11, abstract
                                                               ##          TYPE_MULTISURFACE        = 12, abstract
                                                               ##          TYPE_CURVE               = 13, abstract
                                                               ##          TYPE_SURFACE             = 14, abstract
                                                               ## ```
  SFCGAL_TYPE_TRIANGULATEDSURFACE* = (16).sfcgal_geometry_type_t
  SFCGAL_TYPE_TRIANGLE* = (100).sfcgal_geometry_type_t ## ```
                                                       ##   17 in Wikipedia???
                                                       ## ```
  SFCGAL_TYPE_SOLID* = (101).sfcgal_geometry_type_t ## ```
                                                    ##   17 in Wikipedia???
                                                    ## ```
  SFCGAL_TYPE_MULTISOLID* = (102).sfcgal_geometry_type_t
type
  sfcgal_geometry_t* {.importc, impsfcgal_cHdr.} = object ## ```
                                                          ##   TODO : return of errors ! => error handler
                                                          ##     
                                                          ##   
                                                          ##    Minimal C API for SFCGAL
                                                          ##   
                                                          ##    
                                                          ##     --------------------------------------------------------------------------------------*
                                                          ##   
                                                          ##    Support for SFCGAL::Geometry class hierarchy
                                                          ##   
                                                          ##   --------------------------------------------------------------------------------------
                                                          ##     
                                                          ##    sfcgal_geometry_t is an opaque pointer type that is used to represent a
                                                          ##    pointer to SFCGAL::Geometry
                                                          ##    @ingroup capi
                                                          ## ```
  sfcgal_prepared_geometry_t* {.importc, impsfcgal_cHdr.} = object ## ```
                                                                   ##   --------------------------------------------------------------------------------------*
                                                                   ##   
                                                                   ##    Support for SFCGAL::PreparedGeometry
                                                                   ##   
                                                                   ##   --------------------------------------------------------------------------------------
                                                                   ##     
                                                                   ##    Opaque type that represents the C++ type SFCGAL::PreparedGeometry
                                                                   ##    @ingroup capi
                                                                   ## ```
  srid_t* {.importc, impsfcgal_cHdr.} = uint32
  sfcgal_error_handler_t* {.importc, impsfcgal_cHdr.} = proc (a1: cstring): cint {.
      cdecl, varargs.}
  sfcgal_alloc_handler_t* {.importc, impsfcgal_cHdr.} = proc (a1: uint): pointer {.
      cdecl.}
  sfcgal_free_handler_t* {.importc, impsfcgal_cHdr.} = proc (a1: pointer) {.
      cdecl.}
proc sfcgal_set_geometry_validation*(enabled: cint) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Set the geometry validation mode
                    ##    @ingroup capi
                    ##    @note obsolete
                    ## ```
proc sfcgal_geometry_type_id*(a1: ptr sfcgal_geometry_t): sfcgal_geometry_type_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the type of a given geometry
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_is_valid*(a1: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests if the given geometry is valid or not
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_is_valid_detail*(geom: ptr sfcgal_geometry_t;
                                      invalidity_reason: ptr cstring;
    invalidity_location: ptr ptr sfcgal_geometry_t): cint {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Tests if the given geometry is valid or not
                    ##    And return details in case of invalidity
                    ##    @param geom the input geometry
                    ##    @param invalidity_reason input/output parameter. If non null, a
                    ##    null-terminated string could be allocated and contain reason of the
                    ##    invalidity
                    ##    @param invalidity_location input/output parameter. If non null, a geometry
                    ##    could be allocated and contain the location of the invalidity
                    ##    @ingroup capi
                    ## ```
proc sfcgal_geometry_is_3d*(a1: ptr sfcgal_geometry_t): cint {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Tests if the given geometry is 3D or not
                    ##    @ingroup capi
                    ## ```
proc sfcgal_geometry_is_measured*(a1: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests if the given geometry is measured (has an m) or not
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_is_empty*(a1: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests if the given geometry is empty or not
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_clone*(a1: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns a deep clone of the given geometry
                                    ##    @post returns a pointer to an allocated geometry that must be deallocated by
                                    ##    @ref sfcgal_geometry_delete
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_delete*(a1: ptr sfcgal_geometry_t) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Deletes a given geometry
                    ##    @pre the given pointer must have been previously allocated by a creation
                    ##    function
                    ##    @ingroup capi
                    ## ```
proc sfcgal_geometry_as_text*(a1: ptr sfcgal_geometry_t; buffer: ptr cstring;
                              len: ptr uint) {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                               ##   Returns a WKT representation of the given geometry using CGAL exact integer
                                                                               ##    fractions as coordinate values
                                                                               ##    @post buffer is returned allocated and must be freed by the caller
                                                                               ##    @ingroup capi
                                                                               ## ```
proc sfcgal_geometry_as_text_decim*(a1: ptr sfcgal_geometry_t;
                                    numDecimals: cint; buffer: ptr cstring;
                                    len: ptr uint) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns a WKT representation of the given geometry using floating point
                    ##    coordinate values. Floating point precision can be set via the numDecimals
                    ##    parameter. Setting numDecimals to -1 yields the same result as
                    ##    sfcgal_geometry_as_text.
                    ##    @post buffer is returned allocated and must be freed by the caller
                    ##    @ingroup capi
                    ## ```
proc sfcgal_point_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty point
                    ##    @ingroup capi
                    ## ```
proc sfcgal_point_create_from_xy*(x: cdouble; y: cdouble): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates a point from two X and Y coordinates
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_point_create_from_xyz*(x: cdouble; y: cdouble; z: cdouble): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates a point from three X, Y and Z coordinates
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_point_x*(a1: ptr sfcgal_geometry_t): cdouble {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns the X coordinate of the given Point
                    ##    @pre the given geometry must be a Point
                    ##    @pre the given point must not be empty
                    ##    @ingroup capi
                    ## ```
proc sfcgal_point_y*(a1: ptr sfcgal_geometry_t): cdouble {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns the Y coordinate of the given Point
                    ##    @pre the given geometry must be a Point
                    ##    @pre the given point must not be empty
                    ##    @ingroup capi
                    ## ```
proc sfcgal_point_z*(a1: ptr sfcgal_geometry_t): cdouble {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns the Z coordinate of the given Point
                    ##    @pre the given geometry must be a Point
                    ##    @pre the given point must not be empty
                    ##    @post the Z coordinate can value NaN if the given point is 2D only
                    ##    @ingroup capi
                    ## ```
proc sfcgal_point_m*(a1: ptr sfcgal_geometry_t): cdouble {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns the M coordinate of the given Point
                    ##    @pre the given geometry must be a Point
                    ##    @pre the given point must not be empty
                    ##    @post the M coordinate can value NaN if the given point has no m
                    ##    @ingroup capi
                    ## ```
proc sfcgal_linestring_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty LineString
                    ##    @ingroup capi
                    ## ```
proc sfcgal_linestring_num_points*(linestring: ptr sfcgal_geometry_t): uint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the number of points of the given LineString
                                    ##    @pre linestring must be a LineString
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_linestring_point_n*(linestring: ptr sfcgal_geometry_t; i: uint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the ith point of a given LineString
                                    ##    @param i is the point index in the LineString
                                    ##    @pre linestring must be a LineString
                                    ##    @pre i >= and i < sfcgal_linestring_num_points
                                    ##    @post the returned Point is not writable and must not be deallocated by the
                                    ##    caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_linestring_add_point*(linestring: ptr sfcgal_geometry_t;
                                  point: ptr sfcgal_geometry_t) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Adds a point to a LineString
                           ##    @param linestring is the LineString where the Point has to be added to
                           ##    @param point is the Point to add to the given LineString
                           ##    @pre i >= and i < sfcgal_linestring_num_points
                           ##    @post the ownership of Point is taken by the function
                           ##    @ingroup capi
                           ## ```
proc sfcgal_triangle_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty Triangle
                    ##    @ingroup capi
                    ## ```
proc sfcgal_triangle_create_from_points*(pta: ptr sfcgal_geometry_t;
    ptb: ptr sfcgal_geometry_t; ptc: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates a Triangle from three given Point
                                    ##    @pre pta must be a Triangle
                                    ##    @pre ptb must be a Triangle
                                    ##    @pre ptc must be a Triangle
                                    ##    @post the ownership of the three points are not taken. The caller is still
                                    ##    responsible of their deallocation
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_triangle_vertex*(triangle: ptr sfcgal_geometry_t; i: cint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns one the Triangle's vertex as a Point
                                    ##    @pre triangle must be a Triangle
                                    ##    @pre i >= 0 and i < 3
                                    ##    @post returns a pointer to one of the vertices as a Point. This pointer is
                                    ##    not writable and must not be deallocated by the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_triangle_set_vertex*(triangle: ptr sfcgal_geometry_t; i: cint;
                                 vertex: ptr sfcgal_geometry_t) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Sets one vertex of a Triangle
                           ##    @pre triangle must be a Triangle
                           ##    @pre vertex must be a Point
                           ##    @post returns a pointer to one of the vertices as a Point. This pointer is
                           ##    not writable and must not be deallocated by the caller
                           ##    @ingroup capi
                           ## ```
proc sfcgal_triangle_set_vertex_from_xy*(triangle: ptr sfcgal_geometry_t;
    i: cint; x: cdouble; y: cdouble) {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                       ##   Sets one vertex of a Triangle from two coordinates
                                                                       ##    @pre triangle must be a Triangle
                                                                       ##    @pre i >= 0 and i < 3
                                                                       ##    @ingroup capi
                                                                       ## ```
proc sfcgal_triangle_set_vertex_from_xyz*(triangle: ptr sfcgal_geometry_t;
    i: cint; x: cdouble; y: cdouble; z: cdouble) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Sets one vertex of a Triangle from three coordinates
                    ##    @pre triangle must be a Triangle
                    ##    @pre i >= 0 and i < 3
                    ##    @ingroup capi
                    ## ```
proc sfcgal_polygon_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty Polygon
                    ##    @ingroup capi
                    ## ```
proc sfcgal_polygon_create_from_exterior_ring*(ring: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates an empty Polygon from an extrior ring
                                    ##    @pre ring must be a LineString
                                    ##    @post the ownership of the given ring is taken. The caller is not responsible
                                    ##    anymore of its deallocation
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_polygon_exterior_ring*(polygon: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the exterior ring of a given Polygon
                                    ##    @pre polygon must be a Polygon
                                    ##    @pre polygon must not be empty
                                    ##    @post the returned ring is a LineString, is not writable and must not be
                                    ##    deallocated by the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_polygon_num_interior_rings*(polygon: ptr sfcgal_geometry_t): uint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the number of interior rings of a given Polygon
                                    ##    @pre polygon must be a Polygon
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_polygon_interior_ring_n*(polygon: ptr sfcgal_geometry_t; i: uint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the ith interior ring of a given Polygon
                                    ##    @pre polygon must be a Polygon
                                    ##    @pre i >= 0 and i < sfcgal_polygon_num_interior_rings
                                    ##    @post the returned ring is a LineString, is not writable and must not be
                                    ##    deallocated by the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_polygon_add_interior_ring*(polygon: ptr sfcgal_geometry_t;
                                       ring: ptr sfcgal_geometry_t) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Adds an interior ring to a given Polygon
                           ##    @pre polygon must be a Polygon
                           ##    @pre ring must be a LineString
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_collection_create*(): ptr sfcgal_geometry_t {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Creates an empty  GeometryCollection
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_collection_num_geometries*(
    collection: ptr sfcgal_geometry_t): uint {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                               ##   Returns the number of geometries of a given GeometryCollection
                                                                               ##    @pre collection is a GeometryCollection
                                                                               ##    @ingroup capi
                                                                               ## ```
proc sfcgal_geometry_collection_geometry_n*(collection: ptr sfcgal_geometry_t;
    i: uint): ptr sfcgal_geometry_t {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                      ##   Returns the ith geometry of a GeometryCollection
                                                                      ##    @pre collection is a GeometryCollection
                                                                      ##    @pre i >= 0 and i < sfcgal_geometry_collection_num_geometries
                                                                      ##    @post the returned Geometry is not writable and must not be deallocated by
                                                                      ##    the caller
                                                                      ##    @ingroup capi
                                                                      ## ```
proc sfcgal_geometry_collection_add_geometry*(collection: ptr sfcgal_geometry_t;
    geometry: ptr sfcgal_geometry_t) {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                       ##   Adds a Geometry to a given GeometryCollection
                                                                       ##    @pre collection must be a GeometryCollection
                                                                       ##    @post the ownership of the given geometry is taken. The caller is not
                                                                       ##    responsible anymore of its deallocation
                                                                       ##    @ingroup capi
                                                                       ## ```
proc sfcgal_multi_point_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty MultiPoint
                    ##    @ingroup capi
                    ## ```
proc sfcgal_multi_linestring_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty MultiLineString
                    ##    @ingroup capi
                    ## ```
proc sfcgal_multi_polygon_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty MultiPolygon
                    ##    @ingroup capi
                    ## ```
proc sfcgal_polyhedral_surface_create*(): ptr sfcgal_geometry_t {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Creates an empty PolyhedralSurface
                           ##    @ingroup capi
                           ## ```
proc sfcgal_polyhedral_surface_num_polygons*(polyhedral: ptr sfcgal_geometry_t): uint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the number of polygons of a given PolyhedralSurface
                                    ##    @pre polyhedral must be a PolyhedralSurface
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_polyhedral_surface_polygon_n*(polyhedral: ptr sfcgal_geometry_t;
    i: uint): ptr sfcgal_geometry_t {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                      ##   Returns the ith polygon of a given PolyhedralSurface
                                                                      ##    @pre polyhedral must be a PolyhedralSurface
                                                                      ##    @pre i >= 0 and i < sfcgal_polyhedral_surface_num_polygons(polyhedral)
                                                                      ##    @post the returned Polygon is not writable and must not be deallocated by the
                                                                      ##    caller
                                                                      ##    @ingroup capi
                                                                      ## ```
proc sfcgal_polyhedral_surface_add_polygon*(polyhedral: ptr sfcgal_geometry_t;
    polygon: ptr sfcgal_geometry_t) {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                      ##   Adds a Polygon to a given PolyhedralSurface
                                                                      ##    @pre polyhedral must be a PolyhedralSurface
                                                                      ##    @pre polygon must be a Polygon
                                                                      ##    @post the ownership of the Polygon is taken. The caller is not responsible
                                                                      ##    anymore of its deallocation
                                                                      ##    @ingroup capi
                                                                      ## ```
proc sfcgal_triangulated_surface_create*(): ptr sfcgal_geometry_t {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Creates an empty TriangulatedSurface
                           ##    @ingroup capi
                           ## ```
proc sfcgal_triangulated_surface_num_triangles*(tin: ptr sfcgal_geometry_t): uint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the number of triangles of a given TriangulatedSurface
                                    ##    @pre tin must be a TriangulatedSurface
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_triangulated_surface_triangle_n*(tin: ptr sfcgal_geometry_t; i: uint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the ith Triangle of a given TriangulatedSurface
                                    ##    @pre tin must be a TriangulatedSurface
                                    ##    @pre i >= 0 and i < sfcgal_triangulated_surface_num_triangles( tin )
                                    ##    @post the returned Triangle is not writable and must not be deallocated by
                                    ##    the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_triangulated_surface_add_triangle*(tin: ptr sfcgal_geometry_t;
    triangle: ptr sfcgal_geometry_t) {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                       ##   Adds a Triangle to a given TriangulatedSurface
                                                                       ##    @pre tin must be a TriangulatedSurface
                                                                       ##    @pre triangle must be a Triangle
                                                                       ##    @post the ownership of the Triangle is taken. The caller is not responsible
                                                                       ##    anymore of its deallocation
                                                                       ##    @ingroup capi
                                                                       ## ```
proc sfcgal_solid_create*(): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Creates an empty Solid
                    ##    @ingroup capi
                    ## ```
proc sfcgal_solid_create_from_exterior_shell*(shell: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates a Solid from an exterior shell
                                    ##    @pre ring must be a PolyhedralSurface
                                    ##    @post the ownership of the given shell is taken. The caller is not
                                    ##    responsible anymore of its deallocation
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_solid_num_shells*(solid: ptr sfcgal_geometry_t): uint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the number of shells of a given Solid
                           ##    @pre solid must be a Solid
                           ##    @ingroup capi
                           ## ```
proc sfcgal_solid_shell_n*(solid: ptr sfcgal_geometry_t; i: uint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the ith shell of a given Solid
                                    ##    @pre solid must be a Solid
                                    ##    @pre i >= 0 and i < sfcgal_solid_num_shells( tin )
                                    ##    @post the returned PolyhedralSurface is not writable and must not be
                                    ##    deallocated by the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_solid_add_interior_shell*(solid: ptr sfcgal_geometry_t;
                                      shell: ptr sfcgal_geometry_t) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Adds a shell to a given Solid
                           ##    @pre solid must be a Solid
                           ##    @pre shell must be a PolyhedralSurface
                           ##    @post the ownership of the shell is taken. The caller is not responsible
                           ##    anymore of its deallocation
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_has_validity_flag*(geom: ptr sfcgal_geometry_t): cint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Gets the validity flag of the geometry.
                                    ## ```
proc sfcgal_geometry_force_valid*(geom: ptr sfcgal_geometry_t; valid: cint) {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Sets the validity flag of the geometry.
                                    ##    FIXME We better have geometry constructors to directly build valid geometries
                                    ## ```
proc sfcgal_prepared_geometry_create*(): ptr sfcgal_prepared_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates an empty PreparedGeometry
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_create_from_geometry*(
    geometry: ptr sfcgal_geometry_t; srid: srid_t): ptr sfcgal_prepared_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Creates a PreparedGeometry from a Geometry and an SRID
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_delete*(prepared: ptr sfcgal_prepared_geometry_t) {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Deletes a given PreparedGeometry
                                    ##    @pre prepared must be a PreparedGeometry
                                    ##    @post the underlying Geometry linked to the given PreparedGeometry is also
                                    ##    deleted
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_geometry*(prepared: ptr sfcgal_prepared_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the Geometry associated with a given PreparedGeometry
                                    ##    @pre prepared must be a PreparedGeometry
                                    ##    @post the returned Geometry is not writable and must not be deallocated by
                                    ##    the caller
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_set_geometry*(
    prepared: ptr sfcgal_prepared_geometry_t; geometry: ptr sfcgal_geometry_t) {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Sets the Geometry associated with the given PreparedGeometry
                                    ##    @pre prepared must be a PreparedGeometry
                                    ##    @post the ownership of the given geometry is taken. The caller is not
                                    ##    responsible anymore of its deallocation
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_srid*(prepared: ptr sfcgal_prepared_geometry_t): srid_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns SRID associated with a given PreparedGeometry
                                    ##    @pre prepared must be a PreparedGeometry
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_prepared_geometry_set_srid*(prepared: ptr sfcgal_prepared_geometry_t;
                                        a2: srid_t) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Sets SRID associated with a given PreparedGeometry
                    ##    @pre prepared must be a PreparedGeometry
                    ##    @ingroup capi
                    ## ```
proc sfcgal_prepared_geometry_as_ewkt*(prepared: ptr sfcgal_prepared_geometry_t;
                                       num_decimals: cint; buffer: ptr cstring;
                                       len: ptr uint) {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns an EWKT representation of the given PreparedGeometry
                    ##    @param num_decimals number of decimals. -2 for a variable number of decimals.
                    ##    -1 for an exact representation
                    ##    @post buffer is returned allocated and must be freed by the caller
                    ##    @ingroup capi
                    ## ```
proc sfcgal_io_read_wkt*(a1: cstring; len: uint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   --------------------------------------------------------------------------------------*
                                    ##   
                                    ##    I/O functions
                                    ##   
                                    ##   --------------------------------------------------------------------------------------
                                    ##     
                                    ##    io::readWKT
                                    ## ```
proc sfcgal_io_read_ewkt*(a1: cstring; len: uint): ptr sfcgal_prepared_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
proc sfcgal_io_write_binary_prepared*(a1: ptr sfcgal_prepared_geometry_t;
                                      a2: ptr cstring; a3: ptr uint) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Serialization
                           ##    
                           ##      allocates into char**, must be freed by the caller
                           ## ```
proc sfcgal_io_read_binary_prepared*(a1: cstring; l: uint): ptr sfcgal_prepared_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
proc sfcgal_geometry_intersects*(geom1: ptr sfcgal_geometry_t;
                                 geom2: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   --------------------------------------------------------------------------------------*
                           ##   
                           ##    Spatial processing
                           ##   
                           ##   --------------------------------------------------------------------------------------
                           ##     
                           ##    Tests the intersection of geom1 and geom2
                           ##    @pre isValid(geom1) == true
                           ##    @pre isValid(geom2) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_intersects_3d*(geom1: ptr sfcgal_geometry_t;
                                    geom2: ptr sfcgal_geometry_t): cint {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Tests the 3D intersection of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_intersection*(geom1: ptr sfcgal_geometry_t;
                                   geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the intersection of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_intersection_3d*(geom1: ptr sfcgal_geometry_t;
                                      geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the 3D intersection of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_difference*(geom1: ptr sfcgal_geometry_t;
                                 geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the difference of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_difference_3d*(geom1: ptr sfcgal_geometry_t;
                                    geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the 3D difference of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_union*(geom1: ptr sfcgal_geometry_t;
                            geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the union of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_union_3d*(geom1: ptr sfcgal_geometry_t;
                               geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the 3D union of geom1 and geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_convexhull*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the convex hull of geom
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_convexhull_3d*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the 3D convex hull of geom
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_volume*(geom: ptr sfcgal_geometry_t): cdouble {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the volume of geom (must be a volume)
                           ##    @pre isValid(geom) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_area*(geom: ptr sfcgal_geometry_t): cdouble {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the area of geom
                           ##    @pre isValid(geom) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_area_3d*(geom: ptr sfcgal_geometry_t): cdouble {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the 3D area of geom
                           ##    @pre isValid(geom) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_is_planar*(geom: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests if the given Geometry is planar
                           ##    @pre isValid(geom) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_orientation*(geom: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the orientation of the given Polygon
                           ##    -1 for a counter clockwise orientation
                           ##    1 for a clockwise orientation
                           ##    0 for an invalid or undetermined orientation
                           ##    @pre geom is a Polygon
                           ##    @pre isValid(geom) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_tesselate*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns a tesselation of the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_triangulate_2dz*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns a triangulation of the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_extrude*(geom: ptr sfcgal_geometry_t; ex: cdouble;
                              ey: cdouble; ez: cdouble): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns an extrusion of the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_make_solid*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Convert a PolyhedralSurface to a Solid
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup detail
                                    ## ```
proc sfcgal_geometry_force_lhr*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Force a Left Handed Rule on the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_force_rhr*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Force a Right Handed Rule on the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_distance*(geom1: ptr sfcgal_geometry_t;
                               geom2: ptr sfcgal_geometry_t): cdouble {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Computes the distance of the two given Geometry objects
                           ##    @pre isValid(geom1) == true
                           ##    @pre isValid(geom2) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_distance_3d*(geom1: ptr sfcgal_geometry_t;
                                  geom2: ptr sfcgal_geometry_t): cdouble {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Computes the 3D distance of the two given Geometry objects
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_round*(geom: ptr sfcgal_geometry_t; r: cint): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Round coordinates of the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_minkowski_sum*(geom1: ptr sfcgal_geometry_t;
                                    geom2: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the minkowski sum geom1 + geom2
                                    ##    @pre isValid(geom1) == true
                                    ##    @pre isValid(geom2) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_offset_polygon*(geom: ptr sfcgal_geometry_t;
                                     radius: cdouble): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the offset polygon of the given Geometry.
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_straight_skeleton*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the straight skeleton of the given Geometry
                                    ##    @pre isValid(geom) == true
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_straight_skeleton_distance_in_m*(
    geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.importc, cdecl,
    impsfcgal_cDyn.}
  ## ```
                    ##   Returns the straight skeleton of the given Geometry with the distance to the
                    ##    border as M coordinate
                    ##    @pre isValid(geom) == true
                    ##    @post isValid(return) == true
                    ##    @ingroup capi
                    ## ```
proc sfcgal_geometry_approximate_medial_axis*(geom: ptr sfcgal_geometry_t): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the approximate medial axis for the given Polygon
                                    ##    Approximate medial axis is based on straight skeleton
                                    ##    @pre isValid(geom) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_covers*(geom1: ptr sfcgal_geometry_t;
                             geom2: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests the coverage of geom1 and geom2
                           ##    @pre isValid(geom1) == true
                           ##    @pre isValid(geom2) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_covers_3d*(geom1: ptr sfcgal_geometry_t;
                                geom2: ptr sfcgal_geometry_t): cint {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Tests the 3D coverage of geom1 and geom2
                           ##    @pre isValid(geom1) == true
                           ##    @pre isValid(geom2) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_geometry_line_sub_string*(geom: ptr sfcgal_geometry_t;
                                      start: cdouble; `end`: cdouble): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the substring of the given LineString between fractional distances
                                    ##    @pre isValid(geom) == true
                                    ##    @pre geom is a Linestring
                                    ##    @pre -1 <= start <= 1
                                    ##    @pre -1 <= end <= 1
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_alpha_shapes*(geom: ptr sfcgal_geometry_t; alpha: cdouble;
                                   allow_holes: bool): ptr sfcgal_geometry_t {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Returns the alpha shapes of geom
                                    ##    @pre isValid(geom) == true
                                    ##    @pre alpha >= 0
                                    ##    @post isValid(return) == true
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_geometry_optimal_alpha_shapes*(geom: ptr sfcgal_geometry_t;
    allow_holes: bool; nb_components: uint): ptr sfcgal_geometry_t {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Returns the optimal alpha shapes of geom
                           ##    @pre isValid(geom) == true
                           ##    @pre alpha >= 0
                           ##    @pre nb_components >= 0
                           ##    @post isValid(return) == true
                           ##    @ingroup capi
                           ## ```
proc sfcgal_set_error_handlers*(warning_handler: sfcgal_error_handler_t;
                                error_handler: sfcgal_error_handler_t) {.
    importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                    ##   Sets the error handlers. These callbacks are called on warning or error
                                    ##    @param warning_handler is the printf-styled callback function that will be
                                    ##    called when a function raises a warning. The default behaviour is to call
                                    ##    printf.
                                    ##    @param error_handler is the printf-style callback function that will be
                                    ##    called when a function generates an error. The default behaviour is to call
                                    ##    printf.
                                    ##    @ingroup capi
                                    ## ```
proc sfcgal_set_alloc_handlers*(malloc_handler: sfcgal_alloc_handler_t;
                                free_handler: sfcgal_free_handler_t) {.importc,
    cdecl, impsfcgal_cDyn.}
  ## ```
                           ##   Sets the error handlers. These callbacks are called on warning or error
                           ##    @param malloc_handler is the function to call for memory allocation. The
                           ##    default behaviour is to call malloc()
                           ##    @param free_handler is the function to call for memory deallocation. The
                           ##    default behaviour is to call free()
                           ##    @ingroup capi
                           ## ```
proc sfcgal_init*() {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                      ##   --------------------------------------------------------------------------------------*
                                                      ##   
                                                      ##    Init
                                                      ##   
                                                      ##   --------------------------------------------------------------------------------------
                                                      ##     
                                                      ##    This function must be called before all the other one.
                                                      ##    @ingroup capi
                                                      ## ```
proc sfcgal_version*(): cstring {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                  ##   Get version
                                                                  ##    @ingroup capi
                                                                  ## ```
proc sfcgal_full_version*(): cstring {.importc, cdecl, impsfcgal_cDyn.}
  ## ```
                                                                       ##   Get full version (including CGAL and Boost versions)
                                                                       ##    @ingroup capi
                                                                       ## ```
{.pop.}
