import unittest
import sfcgal

test "init":
  sfcgal_init()

test "versions":
  echo "Version: " & $sfcgal_version()
  check "1.4.1" == $sfcgal_version()
  echo "Full Version: " & $sfcgal_full_version()

test "Point in Polygon":
  var p = sfcgal_point_create_from_xy(2.0, 3.0)
  var line = sfcgal_linestring_create()
  sfcgal_linestring_add_point(line, sfcgal_point_create_from_xy(0.0, 0.0))
  sfcgal_linestring_add_point(line, sfcgal_point_create_from_xy(5.0, 0.0))
  sfcgal_linestring_add_point(line, sfcgal_point_create_from_xy(5.0, 5.0))
  sfcgal_linestring_add_point(line, sfcgal_point_create_from_xy(0.0, 5.0))
  sfcgal_linestring_add_point(line, sfcgal_point_create_from_xy(0.0, 0.0))
  var polyg = sfcgal_polygon_create_from_exterior_ring(line)
  check sfcgal_geometry_intersects(polyg, p) == 1
  check sfcgal_geometry_intersects(p, polyg) == 1
  check sfcgal_geometry_intersects(line, p) == 0
  check sfcgal_geometry_intersects(p, line) == 0

  var p_inter = sfcgal_geometry_intersection(p, polyg)
  check sfcgal_geometry_type_id(p_inter) == SFCGAL_TYPE_POINT
  check sfcgal_geometry_is_empty(p_inter) != 1
  check sfcgal_point_x(p_inter) == sfcgal_point_x(p)
  check sfcgal_point_y(p_inter) == sfcgal_point_y(p)
