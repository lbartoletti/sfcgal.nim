#!/bin/sh

if [[ -z "${INCLUDE_DIR}" ]]; then
  INCLUDE_DIR="/usr/local/include"
fi

mkdir lib
mkdir src
sed -e "s/SFCGAL_API//" ${INCLUDE_DIR}/SFCGAL/capi/sfcgal_c.h > lib/sfcgal_c.h

toast --pnim --dynlib libSFCGAL.so lib/sfcgal_c.h > src/sfcgal.nim

nim r tests/test.nim

if [ $? -eq 0 ]
then
    echo "success"
else
    echo "failure"
fi
