# Package

version       = "0.1.0"
author        = "Loïc Bartoletti"
description   = "SFCGAL nim binding library"
license       = "MIT"
srcDir        = "src"
